# Log reader

Program used to read all kinds of useful information out of game logfiles

Currently limited to only reading names out of chat and combat logs

## Usage

1. Download binary (link coming soon, until that have to build from src)

2. Put binary into logs folder `/home/mcsneaky/.local/share/starconflict/logs/` or wherever it is for you

3. Execute binary

4. Output will be in `names.json`, send it to McSneaky or do you want with it :)

## Contributing

> It's built in Rust. Requires Rust with Cargo to be installed
>
> Easiest is to use `rustup` https://www.rust-lang.org/tools/install

1. Clone it `git clone git@gitlab.com:star-conflict/tooling/log-reader.git`

2. Run `cargo check` to check if everything is intact

3. Code

4. Make merge request describing what you did
