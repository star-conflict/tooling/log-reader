// use std::io;
use std::fs;
use std::io::BufReader;
use std::io::BufRead;
use std::io::prelude::*;
use std::fs::File;
use std::collections::HashSet;

fn main() {
    // println!("Enter path to logs");
    // println!("EX: /home/mcsneaky/.local/share/starconflict/logs/");
    // println!("EX: .");
    
    // Use below to take user input
    // let mut full_path = String::new();
    // io::stdin().read_line(&mut full_path)
    //     .expect("Faled to read user input");

    // let len = full_path.trim_end_matches(&['\r', '\n'][..]).len();
    // full_path.truncate(len);

    // full_path = full_path.trim().to_string();
    // println!("{:?}", full_path);

    let full_path = ".";
    let paths = fs::read_dir(&full_path)
        .expect("Failed to read directory");

    let mut names = HashSet::new();

    for path in paths {
        let path_name = path.unwrap().path().into_os_string().into_string().unwrap();
        // Read only directory contents
        if fs::metadata(&path_name).unwrap().is_dir() {
            let chat_file = path_name.to_owned() + "/chat.log";
            let combat_file = path_name.to_owned() + "/combat.log";
            // let chat_file = path_name.push_str("/chat.log").to_string();
            names_from_chat_log(&chat_file, &mut names);
            names_from_combat_log(&combat_file, &mut names);
        }
    }
    
    // Save output to names.json
    let mut file = File::create("./names.json").unwrap();
    file.write_all(serde_json::to_string(&names).unwrap().as_bytes()).unwrap();
}

/**
 * Parse names out of chat log file
 */
fn names_from_chat_log (file_name: &str, names: &mut HashSet<String>) {
    // Set file pointer
    let file_pointer = File::open(file_name)
        .expect("Failed to open chat log");

    // Start reading from file pointer to buffer
    let file = BufReader::new(&file_pointer);
    // Loop over all lines
    for (_num, raw_line) in file.lines().enumerate() {
        // Actually read line
        let line = raw_line.expect("Failed to read chat log line");
        // Get start and end of patterns
        let start_bytes = line.find(">[").unwrap_or(0);
        let end_bytes = line.find("] ").unwrap_or(0);
        // Check if match was found
        if start_bytes != 0 && end_bytes != 0 {
            // Get result (also add 2 to get rid of >[ at the start)
            let result = &line[start_bytes + 2..end_bytes].trim();
            // Check if name is valid name
            if is_valid(result) {
                // Insert new name to collection
                names.insert(result.to_string());
            }
        }
    }
}

/**
 * Parse names out of combat log file
 */
fn names_from_combat_log (file_name: &str, names: &mut HashSet<String>) {
    let file_pointer = File::open(file_name)
        .expect("Failed to open combat log");
    
    // Start parsing lines
    let file = BufReader::new(&file_pointer);
    for (_num, raw_line) in file.lines().enumerate() {
        let line = raw_line.expect("Faled to read combat log line");
        let mut start_bytes = line.find("| Heal").unwrap_or(0);
        // @TODO: Clear up this mess
        if start_bytes != 0 {
            start_bytes = start_bytes + 6;
        } else {
            start_bytes = line.find("| Damage").unwrap_or(0);
            if start_bytes != 0 {
                start_bytes = start_bytes + 8;
            }
        }
        let mut end_bytes = line.find("|0").unwrap_or(0);
        if end_bytes == 0 {
            end_bytes = line.find("|-0").unwrap_or(0);
        }
        if start_bytes != 0 && end_bytes != 0 {
            let result = &line[start_bytes..end_bytes].trim();
            // Check if found name is valid
            if is_valid(result) {
                names.insert(result.to_string());
            }
        }
    }
}

/**
 * Checks if name is valid
 */
fn is_valid (name: &str) -> bool {
    if !name.chars().all(|c| c.is_ascii_alphanumeric()) {
        println!("Not a valid name: {}", name);
        return false;
    }
    if name.contains("NPC") {
        return false;
    }
    true
}